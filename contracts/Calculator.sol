pragma solidity ^0.4.17;

contract Calculator {
    mapping (string => uint128) results;
    mapping (string => bool) statuses;

    function plusNumber(string key, uint128 i, uint128 j) public returns (uint){
        results[key] = i + j;
        statuses[key] = true;

        return results[key];
    }

    function minusNumber(string key, uint128 i, uint128 j) public returns (uint){
        results[key] = i - j;
        statuses[key] = true;

        return results[key];
    }

    function productNumber(string key, uint128 i, uint128 j) public returns (uint){
        results[key] = i * j;
        statuses[key] = true;

        return results[key];
    }

    function divideNumber(string key, uint128 i, uint128 j) public returns (uint){
        results[key] = i / j;
        if(i % j > 0){
            results[key]++;
        }

        statuses[key] = true;

        return results[key];
    }

    function modNumber(string key, uint128 i, uint128 j) public returns (uint){
        results[key] = i % j;
        statuses[key] = true;

        return results[key];
    }

    function getResult(string key) view public returns (int256){
        if(statuses[key] == true){
            return results[key];
        }
        return -1;
    }
}