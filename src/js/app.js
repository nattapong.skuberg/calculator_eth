App = {
    web3Provider: null,
    contracts: {},

    init: function () {

        return App.initWeb3();
    },

    initWeb3: function () {
        // Is there an injected web3 instance?
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
        } else {
            // If no injected web3 instance is detected, fall back to Ganache
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        }
        web3 = new Web3(App.web3Provider);

        return App.initContract();
    },

    initContract: function () {
        $.getJSON('Calculator.json', function (data) {
            // Get the necessary contract artifact file and instantiate it with truffle-contract
            var CalculatorArtifact = data;
            App.contracts.Calculator = TruffleContract(CalculatorArtifact);

            // Set the provider for our contract
            App.contracts.Calculator.setProvider(App.web3Provider);

            // Use our contract to retrieve and mark the adopted pets
            return /*App.getResult()*/;
        });

        return App.bindEvents();
    },

    bindEvents: function () {
        // $(document).on('click', '.btn-adopt', );

        $(document).on('submit', '#calForm', App.handleCal);
        $(document).on('submit', '#resultForm', App.callResult);
    },

    // getResult: function (results, account) {
    //     var adoptionInstance;
    //
    //     console.log('foo');
    //     App.contracts.Calculator.deployed().then(function (instance) {
    //         adoptionInstance = instance;
    //
    //         return adoptionInstance.getResult.call('a');
    //     }).then(function (results) {
    //         console.log(results);
    //         // for (i = 0; i < adopters.length; i++) {
    //         //     if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
    //         //         $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
    //         //     }
    //         // }
    //         return false;
    //     }).catch(function (err) {
    //         console.log(err.message);
    //         return false;
    //     });
    // },

    handleCal: function (event) {
        event.preventDefault();
        var calculatorInstance;
        var key = $('#key').val();
        var i = parseInt($('#num1').val());
        var j = parseInt($('#num2').val());
        var operator = parseInt($('#operator').val());

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }

            var account = accounts[0];

            App.contracts.Calculator.deployed().then(function (instance) {
                calculatorInstance = instance;
                // Execute adopt as a transaction by sending account
                console.log({'key': key, 'i': i, 'j': j});
                switch (operator) {
                    case 0 :
                        console.log('plusNumber');
                        return calculatorInstance.plusNumber(key, i, j, {from: account});
                    case 1 :
                        console.log('minusNumber');
                        return calculatorInstance.minusNumber(key, i, j, {from: account});
                    case 2 :
                        console.log('productNumber');
                        return calculatorInstance.productNumber(key, i, j, {from: account});
                    case 3 :
                        console.log('divideNumber');
                        return calculatorInstance.divideNumber(key, i, j, {from: account});
                    default :
                        console.log('modNumber');
                        return calculatorInstance.modNumber(key, i, j, {from: account});
                }

            }).then(function (result) {
                console.log(result);
                return false;
            }).catch(function (err) {
                console.log(err.message);
                return false;
            });
        });
    },

    callResult: function (event) {
        event.preventDefault();
        var calculatorInstance;
        var keyResult = $('#keyResult').val();

        App.contracts.Calculator.deployed().then(function (instance) {
            calculatorInstance = instance;
            // Execute adopt as a transaction by sending account
            console.log({'key': keyResult});
            return calculatorInstance.getResult.call(keyResult);
        }).then(function (result) {
            if (result.s != -1){
                console.log(result.c[0]);
                $('#keyResult').val(keyResult);
                $('#result').val(result.c[0]);
            }
            else {
                $('#keyResult').val(keyResult);
                $('#result').val('No data.');
            }

            return false;
        }).catch(function (err) {
            console.log(err.message);
            return false;
        });
    }

};

$(function () {
    $(window).load(function () {
        App.init();
    });
});
